const express = require('express');
const morgan = require('morgan');
const mongoose = require('mongoose');
require('dotenv').config();

const app = express();

mongoose.connect(process.env.MONGO_DB_CONN);

const {authMiddleware} = require('./middleware/authMiddleware');

const {authRouter} = require('./routers/authRouter');
const {notesRouter} = require('./routers/notesRouter');
const {usersRouter} = require('./routers/usersRouter');


app.use(express.json());
app.use(morgan('tiny'));

app.use('/api/auth', authRouter);
app.use('/api/notes', authMiddleware, notesRouter);
app.use('/api/users/me', authMiddleware, usersRouter);

const start = async () => {
  try {
    app.listen(process.env.PORT);
  } catch (err) {
    console.error(`Error on server startup: ${err.message}`);
  }
};

start();

function errorHandler(err, req, res, next) {
  console.error(err);
  res.status(500).send({message: 'Server error'});
}

app.use(errorHandler);
